# QuerySecurityBundle

WIP : Bundle

## Instalation 

 Composer require 
 
 
## Configuration 


File path : `config/packages/query_security.yaml`
```
parameters:
  # All Fields protection
  render_response_on_exception: true # render response instead of Exception (exception in kernel.request event are not catched)
  html_escaped: true # all field are passed to strip_tags function
  denied: ['<script>','javascript', 'http', '//']  # banned word in field value
  escaped: ['redirect']  ## word removed from value

  # per query parameters configuration
  parameters:
     fieldname:
        denied: ['<script>','javascript', 'http', '//']
        
        #Can be a static method or a function set in bootstrap.php
        validator: ['callbackFunctionName']

        #can access submit field if field is array
        subfield:
            html_escaped: false
 ```
 
 
 ## Todo 
 
 [ ] Better Configuration